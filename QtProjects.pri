#--------------------------------------------------------------------------------------#
# QtProjects.pri                                                                       #
# include this file in all your .pro files for added comfort and benefit               #
#                                                                                      #
# 2018-07-31 First version                                                         HS  #
# 2020-06-30 Support static builds through new #define TWSTATICBUILD               HS  #
# 2023-07-19 Introduce a central bin location ../Projects/bin                      HS  #
# 2024-08-22 Switch to a build subdirectory inside the project dir                 HS  #
#--------------------------------------------------------------------------------------#

# --- establish the path to our Project root
defineReplace(projectRoot) {
# first line (default) is for Linux systems
    ProjectPath = /home/henry/Projects
    win32: ProjectPath = C:/Projects
    mac: ProjectPath = /Users/henry/Projects
    return ($$ProjectPath)
}

# returns "TWSTATICBUILD" if we're doing a static build, else returns "TWDYNAMICBUILD"
defineReplace(TWStaticSupport) {
# doing a static build? yes, if any part of target path begins with "Static"
# check using regexp ".*" (means zero or more characters)
    PWDSplit = $$split(OUT_PWD,/)
    contains(PWDSplit,Static.*) { return ("TWSTATICBUILD") }

# no must be dynamic then
    return ("TWDYNAMICBUILD")
}

# --- use these in your DEFINES += statements
# (this allows you to write #include INCLUDELIBNAME in a nice portable way)
# example: DEFINES += $$TWDefineInclude(TWUtils) ---> #include INCLUDETWUTILS in your code
defineReplace(TWDefineInclude) {
    return ("INCLUDE"$$upper($$1)=\\\"$$projectRoot()/lib/$$1/$$1".h"\\\")
}

# example: DEFINES += $$TWDefineInclude2(libsmb2,smb2) ---> #include INCLUDELIBSMB2SMB2
defineReplace(TWDefineInclude2) {
    return ("INCLUDE"$$upper($$1$$2)=\\\"$$projectRoot()/lib/$$1/$$2".h"\\\")
}

# --- use this in your LIBS += statements
# example: LIBS += $$TWLibLine(TWUtils)
# all ./lib/.dll/.dylib/.so files are placed in $ProjectRoot/bin
defineReplace(TWLibLine) {
# return the (fixed) path for dynamic builds
    return (-L$$projectRoot()/bin -l$$1)
}

# conclude with some common code -------------------------------------
# we want c++20 please
CONFIG += c++20

# set the #define for the dynamic/static build flavo
DEFINES += $$TWStaticSupport()

# if we're building a library, set the destdir to $ProjectRoot/bin
equals(TEMPLATE,"lib") { DESTDIR = $$projectRoot()/bin }

# also set the $ProjectRoot/bin as the .dylib/.so linker prefix
# (no effect/harmless on Windows)
QMAKE_SONAME_PREFIX = $$projectRoot()/bin
